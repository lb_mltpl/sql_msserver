
drop database if exists TestDB;

create database TestDB;
use TestDB
go 
--�������� �������������� IDENTITY [ (seed , increment) ] 
--seed - ��������, ������������� ����� ������ ������, ����������� � �������.
--increment - �������� ����������, ������� ������������ � �������� �������������� ���������� ����������� ������.
drop table if exists TestTable

go

create table TestTable (
	[ProductID] [int] identity (1,1) not null,
	[CategoryID] [int] not null, 
	[ProductName] [Varchar] (100) not null,
	[Price] [money] null
	)
go

create table TestTable2 (
	[CategoryID] [int] identity (1,1) not null,
	[CategoryName] [varchar] (100) not null
	)
go

--����������� ������� persisted
create table TestTable3 (
	[ProductId] [int] identity (1,1) not null,
	[ProductName] [varchar] (100) not null,
	[Weight] [decimal] (18,2) null,
	[Price] [Money] null,
	[Summa] as ([weight] * [Price]) persisted
	)
go

--���������� �������

alter table TestTable3 add [SummaDop] as ([Weight] * [Price]) 
persisted;

--Drop table TestTable3

--���������
alter table TestTable alter column [price] [money] not null 
alter table TestTable drop column [price]
alter table TestTable add [price] [money] null

--�������� ��������� �������
create table #TestTable (
	[Id] [int] identity (1,1) not null,
	[ProductName] [varchar] (100) not null,
	[Price] [Money] not null
)
drop table #TestTable

insert into TestTable
	values  (1, '����������', 100),
			(1, '����', 50),
			(2, '�������', 300)
go

insert into TestTable
	values  (3, '������', 100)
		
go

insert into TestTable2
	values  ('������������� ����������'),
			('��������� ����������')
go

select productID, ProductName, Price
from TestTable

--������� 2 ������ ������
select top 2 productID, ProductName, Price
from TestTable

--������� 2 ������������, ������. �� ���� �������� + ������. � ����� ������. ���� 2 ������ 
select top 2 with ties productID, ProductName, Price
from TestTable
order by price desc 

--������� N ������� ������������
select top 20 percent productID, ProductName, Price
from TestTable
order by price desc 

select distinct ProductName, Price 
from TestTable 

--alias
select  t.ProductID as ID,
		t.ProductName as ProductName,
		t.Price as ����
from TestTable as T
-- ���������� ��� 'as'
select  t.ProductID  ID,
		t.ProductName  ProductName,
		t.Price  ����
from TestTable T

--��������� 
select  ProductID,
		ProductName,
		Price
from TestTable
where price > 10 

Select ProductID,
		ProductName,
		Price
from TestTable
where price >= 100 and price <= 500

Select ProductID,
		ProductName,
		Price
from TestTable
where price between 100 and  500

--�������� ������� �
Select ProductID,
		ProductName,
		Price
from TestTable
where ProductName like '�%'

Select ProductID,
		ProductName,
		Price
from TestTable
where Price in (50, 100)

select ProductID,
		ProductName,
		Price
from TestTable
where Price = 100 or ProductName like '����������'

--null �������� null/is not null
select ProductID,
		ProductName,
		Price
from TestTable
where Price is not null

select ProductID,
		ProductName,
		Price
from TestTable
order by 3 desc, 1

select ProductID,
		ProductName,
		Price
from TestTable
order by 3 desc
offset 1 rows

select ProductID,
		ProductName,
		Price
from TestTable
order by 3 desc
offset 1 rows fetch next 1 rows only

--join
select T1.ProductName,
		T2.CategoryName,
		T1.Price
from TestTable T1
inner join TestTable2 T2 on T1.CategoryID = T2.CategoryID
order by T1.CategoryID

select T1.ProductName,
		T2.CategoryName,
		T1.Price
from TestTable T1
left join TestTable2 T2 on T1.CategoryID = T2.CategoryID
order by T1.CategoryID

select T1.ProductName,
		T2.CategoryName,
		T1.Price
from TestTable T1
right join TestTable2 T2 on T1.CategoryID = T2.CategoryID
order by T1.CategoryID

select T1.ProductName,
		T2.CategoryName,
		T1.Price
from TestTable T1
full join TestTable2 T2 on T1.CategoryID = T2.CategoryID
order by T1.CategoryID

--cross join (������������ �����������)
select T1.ProductName,
		T2.CategoryName,
		T1.Price
from TestTable T1
cross join TestTable2 T2 
order by T1.ProductName

--union
select T1.ProductID,
		T1.ProductName,
		T1.Price
from TestTable T1
where T1.ProductId = 1
union
select T1.ProductId,
		T1.ProductName,
		T1.Price
from TestTable T1
where T1.ProductID = 3

--����������
select T2.CategoryName as ��������_���������,
		(select count (*)
		from TestTable
		where CategoryID = T2.CategoryID) as ����������_�������
from TestTable2 T2;

--view
create view ViewCntProducts
as
	select T2.CategoryName as CategoryName,
		(select count(*)
		from TestTable
		where CategoryId = T2.CategoryId) as CntProducts
	from TestTable2 T2

--��������� view = alter
alter view ViewCntProducts
as
	select T2.CategoryID as CategoryID,
	       T2.CategoryName as CategoryName,
		  (select count(*)
		  from TestTable
		   where CategoryId = T2.CategoryId) as CntProducts
	from TestTable2 T2

--drop view ViewCntProducts 

--��������� �������������
select * from sys.tables

select * from sys.columns
where object_id = object_id('TestTable')

--update
update TestTable
set Price = 120 
where ProductID = 1

select * from TestTable
where ProductID = 1

update TestTable set ProductName = 'Te������ �����', price = 150
where ProductID > 3



--update �� �����

update TestTable set ProductName = T2.CategoryName, Price = 200
from TestTable2 T2
inner join TestTable T1 on T1.CategoryID = T2.CategoryID
where ProductID >3
 
insert into TestTable2 (CategoryName)
values ('���� ���������')

--delete (����� ������� ����� ������)/truncate(������� ��� �������� � �������, ������. ������� �������)

delete TestTable
where ProductID > 3

truncate table TestTAble

insert into TestTable
values  (1, '����������', 100),
		(2, '����', 50),
		(3, '�������', 300)

create table TestTable4 (
 ProductID int not null,
 CategoryID int not null,
 ProductName varchar (100) not null,
 Price money null
 )

 insert into TestTable4 
 values (1, 1, '����������', 0),
 (2, 1, '����', 0),
 (4, 1, '����',0)

 select * from TestTable4
select * from TestTable

--merge

--�������
create unique clustered index ix_Clustered on TestTable
(
	ProductId ASC
)
create nonclustered index ix_Nonclustered on TestTable
(
	CategoryId ASC
	)
drop index ix_Nonclustered on TestTable;

create nonclustered index ix_Nonclustered on TestTable
(
	CategoryID ASC,
	ProductName ASC
)
	include (Price);

create nonclustered index ix_Nonclustered on TestTable
(
	CategoryID ASC,
	ProductName ASC
)
	include (Price)
with (drop_existing = on);

--������������� �������
alter index ix_Nonclustered on TestTable
reorganize

--������������ �������
alter index ix_Nonclustered on TestTable
rebuild

--�����������
--not null
alter table TestTable alter column [Price] Money not null
drop table if exists TestTable5
create table TestTable5 (
CategoryID int identity (1,1) not null constraint
PK_CategoryID Primary key,
CategoryName varchar (100) not null
);

create table TestTable6 (
CategoryID int identity (1,1) not null, 
CategoryName varchar (100) not null,
constraint PK_CategoryID_1 Primary key (CategoryID)
);
--��� ����� ������ ������������� (���������), ���� �� ��������
drop table TestTable7
create table TestTable7 (
CategoryID int identity (1,1) not null, 
CategoryName varchar (100) not null,
 Primary key (CategoryID)
);
 
 create table TestTable5 (
						[ProductID] int identity (1,1) not null,
						[CategoryID] int not null,
						[ProductName] varchar (100) not null,
						[Price] money null,
						constraint PK_TestTable5 Primary key (ProductID),
						constraint FK_TestTable5 Foreign key (CategoryID) references TestTable4 (CategoryId)
						on delete cascade
						on update no action
						)

alter table TestTable2 add constraint PK_TestTable2 Primary key (CategoryID);
alter table TestTable add constraint FK_TestTable foreign key (CategoryID) references TestTable2 (CategoryID);

--unique
drop table if exists TestTable6

create table TestTable6 (
	[Column1] int not null constraint PK_TestTable6_C1 unique,
	[Column2] int not null,
	[Column3] int not null,
	constraint PK_TestTable6_C2 unique (Column2)
	);

alter table TestTable6 add constraint PK_TestTable6_C3 unique (Column3);

create table TestTable7 (
	[Column1] int not null,
	[Column2] int not null,
	constraint CK_TestTable7_C1 check (Column1<> 0) 
	);
alter table TestTable7 add constraint CK_TestTAble7_C2 check (Column2 > Column1);

--default
create table TestTable8 (
	[Column1] int null constraint DF_C1 default (1),
	[Column2] int null
	);

alter table TestTable8 add constraint DF_C2 default (2) for Column2;

--�������� �����������
alter table TestTable7 drop constraint CK_TestTable7_C1;
alter table TestTable8 drop constraint DF_C1;

--���������� 
declare @Testvar int
set @Testvar = 10;
select @Testvar*5 as '���������';
--��������� ����������
declare @TestTable table (
	[ProductID] int identity (1,1) not null,
	[CategoryID] int not null,
	[ProductName] varchar (100) not null,
	[Price] money null
	);
	 
	insert into @TestTable 
		select CategoryID, ProductName, Price
		from TestTable 
		where ProductID <= 3;
select * from @TestTable;


/* ������������� 
�����������
*/

--������� ���������
create function TestFunction1
	(
		@ProductId int
	)
returns varchar(100) 
as 
begin 
	declare @ProductName varchar (100);

	select @ProductName = ProductName
	From TestTable 
	where ProductId = @ProductId
return @ProductName
end 
go

select dbo.TestFunction1(1) as [������������ ������];

-- ������� ���������
create function FT_TestFunction
	(
		@CategoryId int
	)
returns table
as
return (
	select ProductId,
			ProductName,
			Price,
			CategoryId
	from TestTable 
	where CategoryId = @CategoryId
	)
go

select * from FT_TestFunction(2);

--������� ��������

create function FT_TestFunction2
	(
	@CategoryId int,
	@Price money
	 )
returns @TMPTable table (
						 ProductId int,
						 ProductName varchar (100),
						 price money,
						 CategoryId int
						 )
as
begin
	if @price < 0 
	set @price = 0
	
	insert into @TMPTable
				select ProductId,
						ProductName,
						price,
						CategoryId
				from TestTable
				where CategoryId = @CategoryId 
				and Price = @Price
	return
end
go

--��������� � ��������
alter function Testfunction1
(
	@ProductId int
)
returns varchar(100)
as 
begin
declare @CategoryName varchar(100);
select @CategoryName = T2.CategoryName
from TestTable T1
join TestTable2 T2 on T1.CategoryId = T2.CategoryId
where T1.ProductId = @ProductId

return @CategoryName
end
go

select ProductId,
		ProductName,
		dbo.Testfunction1(ProductId) as [CategoryName]
from TestTable

drop function Testfunction

--������� �������
declare @Testdate datetime 
set @Testdate = getdate()

select getdate () as [������� ����],
		datename (m, @Testdate) as [�������� ������],
		datepart (m, @Testdate) as [����� ������],
		day (@Testdate) as [����],
		month (@Testdate) as [�����],
		year (@Testdate) as [���],
		datediff (d, '01.01.2018', @Testdate) as [���������� ����],
		dateadd (d, 5, getdate ()) as [+ 5 ����]

--�������������� �������
select 
		abs (-99) as [abs],
		round (1.569136, 4) as [round],
		ceiling (1.6) as [ceiling],
		floor (1.6) as [floor],
		sqrt(16) as [sqrt],
		square (5) as [square],
		power (4, 2) as [power],
		log (10) as [log]
		
--���������
create procedure TestProcedure
(
	@CategoryId int,
	@ProductName varchar (100)
)
as
begin
declare @AVG_Price money
select @AVG_Price = round (AVG(Price), 2)
from TestTable 
where categoryId = @CategoryId

insert into TestTable (CategoryId, ProductName, Price)
	values (@CategoryId, ltrim(rtrim(@ProductName)), @AVG_Price)

	select * from TestTable 
	where CategoryId =@CategoryId
End 

exec TestProcedure @CategoryId = 1, @ProductName ='�������� �����'

--��������
/*�������� ������� ��� ���������*/
create table AutitTestTable (
	Id int identity (1,1) not null,
	DtChange datetime not null,
	Username varchar(100) not null,
	SQL_Command varchar(100) not null,
	ProductId_Old int null,
	ProductId_New int null,
	CategoryId_Old int null,
	CategoryId_New int null,
	ProductName_Old varchar(100) null,
	ProductName_New varchar(100) null,
	Price_Old money null,
	Price_New money null,
	constraint PK_AutitTestTable Primary key (Id)
	)
/*��� �������*/
create trigger TRG_Autit_TestTable on TestTable
	after insert, update, delete
as
begin
declare @SQL_Command varchar(100);
--������������� �������� ����������
if exists (select * from inserted) and not exists (select * from deleted)
	set @SQL_Command = 'insert'

if exists (select *from inserted) and exists (select * from deleted)
	set @SQL_Command = 'update'

if not exists (select * from inserted) and exists (select * from deleted)
	set @SQL_Command = 'delete'
--�� �������� ���������� ��������� ������� ��� ���������
if @SQL_Command = 'update' or @SQL_Command = 'insert'
begin 
	insert into AutitTestTable (DtChange,
								Username,
								SQL_Command,
								ProductId_Old,
								ProductId_New,
								CategoryId_Old,
								CategoryId_New,
								ProductName_Old,
								ProductName_New,
								Price_Old,
								Price_New )
	select getdate(), suser_sname(), @SQL_Command, d.ProductId, i.ProductId, d.CategoryId, i.CategoryId, d.ProductName, i.ProductName, d.Price, i.Price
	from inserted i
	left join deleted d on i.ProductID = d.ProductID
end
if @SQL_Command = 'delete'
begin
	insert into AutitTestTable (DtChange,
								Username,
								SQL_Command,
								ProductId_Old,
								ProductId_New,
								CategoryId_Old,
								CategoryId_New,
								ProductName_Old,
								ProductName_New,
								Price_Old,
								Price_New )
	select getdate(), suser_sname(), @SQL_Command, d.ProductId, null, d.CategoryId, null, d.ProductName, null, d.Price, null
	from deleted d
end
end
	
--�������� ��������
insert into TestTable
	values (1, 'New thing', 0)
update TestTable set ProductName = '������������ ������',
					 Price = 177
where ProductName = 'New thing'
 
delete TestTable where ProductName = '������������ ������'

select * from autitTesttable 

--���������/���������� ��������
disable trigger TRG_Autit_TestTable on TestTable
enable trigger TRG_Autit_TestTable on TestTable

--��������� �������, �� ������� �������� ����� 
create trigger TRG_Autit_TestTable_up_i on TestTable
	after insert, update, delete
as
begin
declare @SQL_Command varchar(100);
--������������� �������� ����������
if exists (select * from inserted) and not exists (select * from deleted)
	set @SQL_Command = 'insert'

if exists (select *from inserted) and exists (select * from deleted)
	set @SQL_Command = 'update'

if not exists (select * from inserted) and exists (select * from deleted)
	set @SQL_Command = 'delete'
--�� �������� ���������� ��������� ������� ��� ���������
if @SQL_Command = 'update' or @SQL_Command = 'insert'
begin 
	insert into AutitTestTable (DtChange,
								Username,
								SQL_Command,
								ProductId_Old,
								ProductId_New,
								CategoryId_Old,
								CategoryId_New,
								ProductName_Old,
								ProductName_New,
								Price_Old,
								Price_New )
	select getdate(), suser_sname(), @SQL_Command, d.ProductId, i.ProductId, d.CategoryId, i.CategoryId, d.ProductName, i.ProductName, d.Price, i.Price
	from inserted i
	left join deleted d on i.ProductID = d.ProductID
end
if @SQL_Command = 'delete'
begin
	insert into AutitTestTable (DtChange,
								Username,
								SQL_Command,
								ProductId_Old,
								ProductId_New,
								CategoryId_Old,
								CategoryId_New,
								ProductName_Old,
								ProductName_New,
								Price_Old,
								Price_New )
	select getdate(), suser_sname(), @SQL_Command, d.ProductId, null, d.CategoryId, null, d.ProductName, null, d.Price, null
	from deleted d
end
end

alter trigger TRG_Autit_TestTable_up_i on TestTable
	after insert, update
as
begin
declare @SQL_Command varchar(100);
--������������� �������� ����������
if exists (select * from inserted) and not exists (select * from deleted)
	set @SQL_Command = 'insert'

else 
	set @SQL_Command = 'update'

--�� �������� ���������� ��������� ������� ��� ���������
if @SQL_Command = 'update' or @SQL_Command = 'insert'
begin 
	insert into AutitTestTable (DtChange,
								Username,
								SQL_Command,
								ProductId_Old,
								ProductId_New,
								CategoryId_Old,
								CategoryId_New,
								ProductName_Old,
								ProductName_New,
								Price_Old,
								Price_New )
	select getdate(), suser_sname(), @SQL_Command, d.ProductId, i.ProductId, d.CategoryId, i.CategoryId, d.ProductName, i.ProductName, d.Price, i.Price
	from inserted i
	left join deleted d on i.ProductID = d.ProductID
end
end

--�������� drop trigger TRG_Autit_TestTable_up_i

--with �����.�������. ���������
with TestCTE as
	(
	select ProductId, ProductName, Price 
	from TestTable
	where CategoryId = 1
	)

select * from TestCTE

