create database airline
go 
use airline
go
create schema airflights
go
drop table if exists airline_2 
create table airline_2 (
						ARILINE_ICAO_CODE varchar(50),	
						ARILINE_NAME varchar(50),
						ARILINE_CALL_SIGN varchar(20),
						AIRLINE_COUNTRY varchar(50),
						FLIGHT_CODE varchar(20),
						AIRPORT_FROM_IATA_CODE varchar(20),
						AIRPORT_FROM_NAME varchar(50),
						AIRPORT_FROM_LOCATION varchar(100),
						AIRPORT_TO_IATA_CODE varchar(20),
						AIRPORT_TO_NAME varchar(100),
						AIRPORT_TO_LOCATION varchar(100),
						FLIGHT_DISTANCE int,
						PILOT_NAME varchar(50),
						PILOT_SURNAME varchar(50),
						COPILOT_NAME varchar(50), 
						COPILOT_SURNAME varchar(50),
						PASSENGER_FIRST_NAME varchar(50), 
						PASSENGER_LAST_NAME varchar(50),
						PASSENGER_COUNTRY varchar(50),
						TRAVEL_CLASS varchar(30),
						TICKET_CODE varchar(50),
						TICKET_PRICE int
) 
go
/* �� �������� postgesql
copy into dbo.[airline_2] (ARILINE_ICAO_CODE, ARILINE_NAME, ARILINE_CALL_SIGN,
				AIRLINE_COUNTRY, FLIGHT_CODE, AIRPORT_FROM_IATA_CODE,
				AIRPORT_FROM_NAME, AIRPORT_FROM_LOCATION, AIRPORT_TO_IATA_CODE,
				AIRPORT_TO_NAME, AIRPORT_TO_LOCATION, FLIGHT_DISTANCE,
				PILOT_NAME, PILOT_SURNAME, COPILOT_NAME, COPILOT_SURNAME,
				PASSENGER_FIRST_NAME, PASSENGER_LAST_NAME, PASSENGER_COUNTRY,
				TRAVEL_CLASS, TICKET_CODE, TICKET_PRICE)
from 'C:\!db\learning db\airflights_passengers_denorm_export.csv'
delimiter ';'
csv header; */

bulk insert dbo.[airline_2]
from 'C:\!db\learning db\airflights_passengers_denorm_export.csv'
with (fieldterminator = ';', rowterminator = '\n');

--airline_info
alter table airline_info drop constraint PK_Flight_Code; 
drop table if exists airline_info; 
create table airline_info (ARILINE_ICAO_CODE varchar(50),	
					  ARILINE_NAME varchar(50),
				      ARILINE_CALL_SIGN varchar(20),
					  AIRLINE_COUNTRY varchar(50),
					  FLIGHT_CODE varchar(20),
					  constraint PK_Flight_Code primary key (FLIGHT_CODE)
					  );

insert into airline_info
select distinct ARILINE_ICAO_CODE,	
				ARILINE_NAME,
				ARILINE_CALL_SIGN,
				AIRLINE_COUNTRY,
				FLIGHT_CODE
from airline_2;

--passengers
alter table passengers drop constraint PK_Ticket_Code; 
drop table if exists passengers;
create table passengers (
						ID_PASS int identity (1,1) not null,
						TICKET_CODE varchar(50),
						PASSENGER_FIRST_NAME varchar(50), 
						PASSENGER_LAST_NAME varchar(50),
						PASSENGER_COUNTRY varchar(50),
						constraint PK_Id_Pass primary key (ID_PASS),
						constraint FK_Ticket_Code foreign key (TICKET_CODE) references tickets (TICKET_CODE)
						--constraint PK_Ticket_Code primary key (TICKET_CODE)
);

insert into passengers (TICKET_CODE,
				PASSENGER_FIRST_NAME, 
				PASSENGER_LAST_NAME,
				PASSENGER_COUNTRY)
select distinct TICKET_CODE,
				PASSENGER_FIRST_NAME, 
				PASSENGER_LAST_NAME,
				PASSENGER_COUNTRY
from airline_2 
where TICKET_CODE is not null;

--tickets
drop table if exists tickets
create table tickets (  TRAVEL_CLASS varchar(30),
					    TICKET_CODE varchar(50),
						TICKET_PRICE int, 
						FLIGHT_CODE varchar(20),
						constraint PK_Ticket_Code primary key (TICKET_CODE),
						
						constraint FK_Flight_Code foreign key (FLIGHT_CODE) references airline_info (FLIGHT_CODE)
						);
insert into tickets ( TRAVEL_CLASS ,
					  TICKET_CODE ,
					  TICKET_PRICE , 
					  FLIGHT_CODE)
select distinct TRAVEL_CLASS ,
				TICKET_CODE ,
				TICKET_PRICE , 
				FLIGHT_CODE
from airline_2 
where TICKET_CODE is not null;

--pilots
drop table if exists pilots
create table pilots   ( ID_Pilots int identity (1,1) not null,
						PILOT_NAME varchar(50),
						PILOT_SURNAME varchar(50),
						COPILOT_NAME varchar(50), 
						COPILOT_SURNAME varchar(50),
						FLIGHT_CODE varchar(20),
						constraint PK_Id_Pilots primary key (ID_Pilots),
						constraint FK_Flight_Code_1 foreign key (FLIGHT_CODE) references airline_info (FLIGHT_CODE)
						);
insert into pilots (PILOT_NAME,
					PILOT_SURNAME,
					COPILOT_NAME, 
					COPILOT_SURNAME,
					FLIGHT_CODE)
select distinct  PILOT_NAME,
				 PILOT_SURNAME,
				 COPILOT_NAME, 
				 COPILOT_SURNAME,
				 FLIGHT_CODE
from airline_2;

--airports
create table airports (AIRPORT_CODE varchar(20) not null,
						AIRPORT_NAME varchar(50) not null,
						AIRPORT_LOCATION varchar(100) not null,
						constraint PK_Airport_Code primary key (AIRPORT_CODE)
						);

insert into airports (AIRPORT_CODE,
					  AIRPORT_NAME,
					  AIRPORT_LOCATION)
select distinct AIRPORT_FROM_IATA_CODE, AIRPORT_FROM_NAME, AIRPORT_FROM_LOCATION from airline_2
union 
select distinct AIRPORT_TO_IATA_CODE, AIRPORT_TO_NAME, AIRPORT_TO_LOCATION from airline_2;


--flights
create table flights (AIRPORT_FROM_IATA_CODE varchar(20),
						AIRPORT_TO_IATA_CODE varchar(20),
						FLIGHT_CODE varchar(20),
						FLIGHT_DISTANCE int,
						primary key (FLIGHT_CODE),
						constraint  FK_FLIGHT_CODE_2 foreign key (FLIGHT_CODE) references airline_info (FLIGHT_CODE),
						
FOREIGN KEY (AIRPORT_FROM_IATA_CODE) REFERENCES airports (AIRPORT_CODE),
FOREIGN KEY (AIRPORT_TO_IATA_CODE) REFERENCES airports (AIRPORT_CODE)
);

INSERT INTO flights (AIRPORT_FROM_IATA_CODE, AIRPORT_TO_IATA_CODE, FLIGHT_CODE, FLIGHT_DISTANCE)
SELECT DISTINCT AIRPORT_FROM_IATA_CODE, AIRPORT_TO_IATA_CODE, FLIGHT_CODE, FLIGHT_DISTANCE FROM airline_2;
